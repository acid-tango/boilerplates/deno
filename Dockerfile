FROM hayd/alpine-deno:1.7.2

ENTRYPOINT []

CMD ["deno", "run", "https://deno.land/std/examples/welcome.ts"]
