import { expect } from "../mod.ts"
import { Main } from "./main.ts"

Deno.test("greets when called", () => {
  const main = new Main("Hello world")
  expect(main.getParam()).toBe("Hello world")
})
