all: lint test

lint:
	deno lint --unstable

test:
	deno test

compile:
	deno compile --unstable src/mod.ts output
